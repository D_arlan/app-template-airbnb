import React, { Component } from 'react';

import {
    View,
    Text,
    StyleSheet,
    SafeAreaView,
    TextInput,
    Platform,
    StatusBar,
    ScrollView,
    Image,
    Dimensions,
    Animated
} from 'react-native';

import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';

import Category from './components/Explore/Category'
import Home from './components/Explore/Home'
import Tag from './components/Explore/Tag'


// import { Container } from './styles';

const { height, width } = Dimensions.get('window')

export default class Explore extends Component {
    componentWillMount() {
        this.scrollY = new Animated.Value(0)
        this.startHeaderHeight = 80
        this.endHeaderHeight = 50
        if (Platform.OS == 'android') {
            this.startHeaderHeight = 100 + StatusBar.currentHeight
            this.endHeaderHeight = 70 + StatusBar.currentHeight
        }
        this.animatedHeaderHeight = this.scrollY.interpolate({
            inputRange: [0, 50],
            outputRange: [this.startHeaderHeight, this.endHeaderHeight],
            extrapolate: 'clamp'
        })
        this.animatedOpacity = this.animatedHeaderHeight.interpolate({
            inputRange: [this.endHeaderHeight, this.startHeaderHeight],
            outputRange: [0, 1],
            extrapolate: 'clamp'
        })
        this.animatedTagTop = this.animatedHeaderHeight.interpolate({
            inputRange: [this.endHeaderHeight, this.startHeaderHeight],
            outputRange: [-30, 10],
            extrapolate: 'clamp'
        })
    }
    render() {
        return (
            <SafeAreaView style={{ flex: 1 }}>
                <View style={{ flex: 1 }}>
                    <Animated.View style={{
                        height: this.animatedHeaderHeight,
                        backgroundColor: '#45395C',
                        borderBottomWidth: 1,
                        borderBottomColor: '#dddddd',
                    }}>
                        <View style={{
                            flexDirection: 'row',
                            padding: 10,
                            backgroundColor: 'white',
                            borderRadius: 50,
                            marginHorizontal: 20,
                            shadowOffset: { width: 0, height: 0 },
                            shadowColor: 'black',
                            shadowOpacity: 0.2,
                            elevation: 1,
                            marginTop: Platform.OS == 'android' ? 30 : null
                        }}>
                            <FontAwesome5 name={'search'} size={24} light />
                            <TextInput
                                underlineColorAndroid="transparent"
                                placeholder="Pesquisar"
                                placeholderTextColor="grey"
                                style={{
                                    flex: 1,
                                    fontWeight: '700',
                                    backgroundColor: 'white',
                                    paddingLeft: 10
                                }}
                            />

                        </View>
                        <Animated.View
                            style={{
                                flexDirection: 'row',
                                marginHorizontal: 20,
                                position: 'relative',
                                top: this.animatedTagTop,
                                opacity: this.animatedOpacity
                            }}
                        >
                            <Tag name='Tag 1' />
                            <Tag name='Tag 2' />
                            <Tag name='Tag 3' />
                            <Tag name='Tag 4 ' />
                        </Animated.View>
                    </Animated.View>
                    <ScrollView
                        scrollEventThrottle={16}
                        onScroll={Animated.event(
                            [
                                {
                                    nativeEvent: { contentOffset: { y: this.scrollY } }
                                }
                            ]
                        )}
                    >
                        <View style={{ flex: 1, backgroundColor: 'white' }}>
                            <Text style={{
                                fontSize: 24,
                                fontWeight: '700',
                                paddingHorizontal: 20
                            }}>
                                Lorem ipsum
                            </Text>

                            <View style={{
                                height: 130,
                                marginTop: 20
                            }}>
                                <ScrollView horizontal={true} showsHorizontalScrollIndicator={false}>
                                    <Category imageUri={require('../assets/bg.jpg')} name="HOMED" />
                                    <Category imageUri={require('../assets/bg.jpg')} name="HOMED" />
                                    <Category imageUri={require('../assets/bg.jpg')} name="HOMED" />
                                    <Category imageUri={require('../assets/bg.jpg')} name="HOMED" />
                                </ScrollView>

                            </View>
                            <View style={{
                                marginTop: 40,
                                paddingHorizontal: 20
                            }}>
                                <Text style={{ fontSize: 24, fontWeight: '700' }}>
                                    Lorem Ipsum dolor
                                </Text>
                                <Text style={{ marginTop: 10, fontWeight: '100' }}>
                                    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin bibendum et odio a fringilla.
                                </Text>
                                <View style={{
                                    width: width - 40,
                                    height: 200,
                                    marginTop: 20
                                }}>
                                    <Image
                                        style={{
                                            flex: 1,
                                            height: null,
                                            width: null,
                                            resizeMode: 'cover',
                                            borderRadius: 5,
                                            borderWidth: 1,
                                            borderColor: '#dddddd'
                                        }} source={require('../assets/bg.jpg')} />
                                </View>
                            </View>
                        </View>
                        <View style={{
                            marginTop: 40
                        }}>
                            <Text style={{ fontSize: 24, fontWeight: '700', paddingHorizontal: 20 }}>
                                Lorem ipsum dolor sit amet
                            </Text>
                            <View style={{
                                paddingHorizontal: 20,
                                marginTop: 20,
                                flexDirection: 'row',
                                flexWrap: 'wrap',
                                justifyContent: 'space-between'
                            }}>
                                <Home
                                    width={width}
                                    preco={90}
                                    rating={4}
                                />
                                <Home
                                    width={width}
                                    preco={90}
                                    rating={4}
                                />
                                <Home
                                    width={width}
                                    preco={90}
                                    rating={4}
                                />

                            </View>
                        </View>
                    </ScrollView>
                </View>
            </SafeAreaView>
        );
    }
}
