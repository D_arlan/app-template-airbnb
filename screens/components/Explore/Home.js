import React, { Component } from 'react';

import { View, Text, Image } from 'react-native';

import StarRating from 'react-native-star-rating'

// import { Container } from './styles';

export default class Home extends Component {
    render() {
        return (
            <View style={{
                width: this.props.width / 2 - 30,
                height: this.props.width / 2 - 30,
                borderColor: '#dddddd',
                borderWidth: 0.5,
                marginBottom: 10
            }}>
                <View style={{ flex: 1 }}>
                    <Image
                        style={{
                            flex: 1,
                            height: null,
                            width: null,
                            resizeMode: 'cover',
                        }} source={require('../../../assets/bg.jpg')} />
                </View>
                <View style={{
                    flex: 1,
                    alignItems: 'flex-start',
                    justifyContent: 'space-evenly',
                    paddingLeft: 10
                }}>

                    <Text style={{ fontSize: 10, color: '#b63838' }}>LOREM IPSUMss</Text>
                    <Text style={{ fontSize: 12, fontWeight: 'bold' }}>Ceará</Text>
                    <Text style={{ fontSize: 10 }}>R${this.props.preco}</Text>
                    <StarRating
                        disabled={true}
                        maxStars={5}
                        rating={this.props.rating}
                        starSize={10}
                    />
                </View>
            </View>
        );
    }
}
