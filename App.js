import React from 'react';
import { StyleSheet, Text, View } from 'react-native';

import { createBottomTabNavigator } from 'react-navigation'


import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';

import Explore from './screens/Explore'
import Trips from './screens/Trips'
import Saved from './screens/Saved'
import Inbox from './screens/Inbox'
import Profile from './screens/Profile'



export default createBottomTabNavigator({
  Explore: {
    screen: Explore,
    navigationOptions: {
      tabBarLabel: 'EXPLORE',
      tabBarIcon: ({ tintColor }) => (
        <FontAwesome5 name={'search'} size={24} color={tintColor} light />
      )
    }
  },
  Saved: {
    screen: Saved,
    navigationOptions: {
      tabBarLabel: 'SAVED',
      tabBarIcon: ({ tintColor }) => (
        <FontAwesome5 name={'heart'} size={24} color={tintColor} light />
      )
    }

  },
  Trips: {
    screen: Trips,
    navigationOptions: {
      tabBarLabel: 'TRIPS',
      tabBarIcon: ({ tintColor }) => (
        <FontAwesome5 name={'hands-helping'} size={24} color={tintColor} light />
      )
    }

  },
  Inbox: {
    screen: Inbox,
    navigationOptions: {
      tabBarLabel: 'INBOX',
      tabBarIcon: ({ tintColor }) => (
        <FontAwesome5 name={'comment'} size={24} color={tintColor} light />

      )
    }

  },
  Profile: {
    screen: Profile,
    navigationOptions: {
      tabBarLabel: 'PERFIL',
      tabBarIcon: ({ tintColor }) => (
        <FontAwesome5 name={'user'} size={24} color={tintColor} light />

      )
    }

  }
},
  {
    tabBarOptions: {
      activeTintColor: 'red',
      inactiveTintColor: 'grey',
      style: {
        backgroundColor: '#fff',
        borderTopWidth: 0,
        shadowOffset: {
          width: 5,
          height: 3,
        },
        shadowColor: 'black',
        ShadowOpacity: 0.5,
        elevation: 5
      }
    }
  })
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
